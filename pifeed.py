#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# -------------------------------------------------------
# piFeed - An open source Raspberry Pi RSS News presenter
# -------------------------------------------------------
# https://gitlab.com/pifeed/pifeed
# -------------------------------------------------------
# Copyright (c) 2017-2018 Thomas Abrahamsson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


import os
import os.path
import pygame, sys, qrcode, urllib
from pygame.locals import *
import numpy,time
from itertools import chain
from datetime import datetime
import feedparser
from HTMLParser import HTMLParser
from qrcode import *
import socket,fcntl,struct
import hashlib
import pprint
import re


# ============================================================
# Enter the ID of the piFeed unit in the text file my_id.txt
# ============================================================
myID = (open('my_id.txt', 'r')).read().strip()


from feed_definitions import unit_feed_filter, unit_screens, all_rss_feeds, my_default_background

screen_width          = unit_screens[myID][0]
screen_height         = unit_screens[myID][1]
hourglass_image_name  = "resources/pictures/hourglass.png"
shadowybg_image_name  = "resources/pictures/shade.png"
loading_image_name    = "resources/backgrounds/startimg.jpg"
feedloops             = 1
news_display_time     = 8
feed_display_time     = 0
fullscreen_mode       = True
font_scale_factor     = (screen_width / 700)
overlay_image_name    = "resources/foregrounds/overlay.png"
titlerow_font_path    = "resources/fonts/Roboto-Medium.ttf"
titlerow_font_size    = (int(font_scale_factor * 46))
bodytext_font_path    = "resources/fonts/Roboto-Medium.ttf"
bodytext_font_size    = (int(font_scale_factor * 20))
headline_font_path    = "resources/fonts/Roboto-Bold.ttf"
headline_font_size    = (int(font_scale_factor * 18))
clockitm_font_path    = "resources/fonts/Roboto-Bold.ttf"
clockitm_font_size    = (int(font_scale_factor * 16))
rss_current_feed  = -1
news_node         = -1
next_node_display = 0
nextrss_change    = 0
refresh_headlines = True
dirty_areas = []
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GRAY  = (160,160,160)
RED   = (255, 0, 0)
angleRoto = 1.0
cached_bg = None
news_nodes = []

re_pattern = re.compile(u'[^\u0000-\uD7FF\uE000-\uFFFF]', re.UNICODE)


# ============================================================

my_filtered_feeds = unit_feed_filter[myID]
rss_feeds = []
i=0

for thisFeed in all_rss_feeds:
   if thisFeed[0] in my_filtered_feeds:
      rss_feeds.append(all_rss_feeds[i])
   i=i+1

print "* Starting piFeed unit " + myID

def repaintBackground():
   global img_bg, cached_bg, dirty_areas
   if (cached_bg == None):
       windowSurface.blit(img_bg, image_bg_rect)
       windowSurface.blit(img_fg, image_fg_rect)
   else:
      for dirtyRect in dirty_areas:
         windowSurface.blit(cached_bg, (dirtyRect.x,dirtyRect.y), dirtyRect)
       #pygame.draw.rect(windowSurface, RED, dirtyRect, 3);

   if (cached_bg == None):
       cached_bg = pygame.Surface((screen_width, screen_height))
       cached_bg.blit(windowSurface, (0,0))
   dirty_areas = []


def showLoading(msg, submsg):
   global hourglass,img_bg,image_bg_rect,shadowbg
   titlerowFont = titlerowFont1
   windowSurface.fill((0,0,0))
   windowSurface.blit(img_bg, image_bg_rect)
   for i in range(0, 2):
      windowSurface.blit(shadowbg,
                      (windowSurface.get_rect().centerx-int(shadowbg.get_width()/2),
                      (windowSurface.get_rect().centery-int(shadowbg.get_height()/2))))
   text = titlerowFont.render(msg, True, BLACK)
   textrect = text.get_rect()
   textrect.centerx = windowSurface.get_rect().centerx
   textrect.centery = windowSurface.get_rect().centery
   windowSurface.blit(text, textrect)
   text2 = titlerowFont.render(msg, True, WHITE)
   windowSurface.blit(text2, map(lambda x: int(x-2), textrect))
   windowSurface.blit(hourglass, (screen_width-hourglass.get_width()-8,0))
   firstlineh = int((text.get_height()))
   text = clockitmFont.render(submsg, True, BLACK)
   textrect = text.get_rect()
   textrect.centerx = windowSurface.get_rect().centerx
   textrect.centery = windowSurface.get_rect().centery+firstlineh
   windowSurface.blit(text, textrect)
   text2 = clockitmFont.render(submsg, True, GRAY)
   windowSurface.blit(text2, map(lambda x: int(x-1), textrect))
   pygame.display.update()

def startImage(loading_image_name, screen_width, screen_height):
    global img_bg
    print "* Loading start image background " + loading_image_name
    try:
        img_start = pygame.image.load(loading_image_name)
    except pygame.error, message:
        print 'Cannot load image:', loading_image_name
        raise SystemExit, message
    img_bg = pygame.transform.scale(img_start, (screen_width, screen_height))
    windowSurface.blit(img_bg, (screen_width, screen_height))


# create a subclass and override the handler methods
lastImgFound = ""
class MyHTMLParser(HTMLParser):
    def reset(self):
        HTMLParser.reset(self)
        self.extracting = False
        self.links      = []
    def handle_starttag(self, tag, attrs):
	if (tag == "img"):
		for attr in attrs:
		  if (attr[0]=='src'):
			  self.links.append(attr[1])

def truncline(t, font, maxwidth):
        text = unicode(t)
        if (len(text) > 300):
            text = text[:300] + "..."
#        print text
        real=len(text)
        stext=text
        l=font.size(text)[0]
        cut=0
        a=0
        done=1
        old = None
        while l > maxwidth:
            a=a+1
            n=text.rsplit(None, a)[0]
            if stext == n:
                cut += 1
                stext= n[:-cut]
            else:
                stext = n
            l=font.size(stext)[0]
            real=len(stext)
            done=0
        return real, done, stext

def wrapline(text, font, maxwidth):
    done=0
    wrapped=[]
    while not done:
        nl, done, stext=truncline(text, font, maxwidth)
        wrapped.append(stext.strip())
        text=text[nl:]
    return wrapped

def wrap_multi_line(text, font, maxwidth):
    """ returns text taking new lines into account.
    """
    lines = chain(*(wrapline(line, font, maxwidth) for line in text.splitlines()))
    return list(lines)

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def get_ip_address(ifname):
    try:
       s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
       return socket.inet_ntoa(fcntl.ioctl(
           s.fileno(),
           0x8915,  # SIOCGIFADDR
           struct.pack('256s', ifname[:15])
       )[20:24])
    except IOError:
       return ""

# set up pygame
pygame.init()

myIP = myID + " @ " + (get_ip_address('eth0') + " " + get_ip_address('wlan0')).strip()

class screenInit :
    screen = None;

    def __init__(self, fullscreen, window_width, window_height):
        "Ininitializes a new pygame screen using the framebuffer"
        # Based on "Python GUI in Linux frame buffer"
        # http://www.karoltomala.com/blog/?p=679
        disp_no = os.getenv("DISPLAY")
        if disp_no:
            print "I'm running under X display = {0}".format(disp_no)

        # Check which frame buffer drivers are available
        # Start with fbcon since directfb hangs with composite output
        drivers = ['fbcon', 'directfb', 'svgalib']
        found = False
        for driver in drivers:
            # Make sure that SDL_VIDEODRIVER is set
            if not os.getenv('SDL_VIDEODRIVER'):
                os.putenv('SDL_VIDEODRIVER', driver)
            try:
                pygame.display.init()
            except pygame.error:
                print 'Driver: {0} failed.'.format(driver)
                continue
            found = True
            break

        if not found:
            raise Exception('No suitable video driver found!')

        size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
        print "* At start, video framebuffer size is %d x %d" % (size[0], size[1])
        print "* Setting video mode: " + str(window_width)+"x"+str(window_height)
        fullFlag = 0

        if (fullscreen == True):
           fullFlag = pygame.FULLSCREEN

        self.screen = pygame.display.set_mode((screen_width,screen_height), fullFlag)

        # Clear the screen to start
        self.screen.fill((0, 0, 0))

        # Initialise font support
        pygame.font.init()

        # Render the screen
        pygame.display.update()

    def __del__(self):
        "Destructor to make sure pygame shuts down, etc."

    def test(self):
        for i in range(0, 32):
            self.screen.fill((255-i*8,255-i*8,255-i*8))
            pygame.display.update()
            time.sleep(0.02)
    
print "* Starting framebuffer mode..."

# Create an instance of the ScreenInit class
screenIniter = screenInit(fullscreen_mode, screen_width, screen_height)
screenIniter.test()
pygame.mouse.set_visible(False)

# ...give me the biggest 32 bit display available

modes = pygame.display.list_modes(32)
if not modes:
 print '* 32bit not supported'
else:
 print '* Supported video modes: ', modes


windowSurface = screenIniter.screen;
size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
screen_width  = size[0]
screen_height = size[1]
img_bg        = pygame.Surface((screen_width, screen_height))
image_bg_rect = img_bg.get_rect()

def loadBackground(newImgName):
    global background_image_name, img_bg, image_bg_rect
    print "* Loading background " + newImgName
    background_image_name = newImgName
    # load the new background image
    try:
        img_bg = pygame.image.load(background_image_name)
    except pygame.error, message:
        print 'Cannot load image:', background_image_name
        raise SystemExit, message
    img_bg        = pygame.transform.scale(img_bg, (screen_width, screen_height))
    image_bg_rect = img_bg.get_rect()

# Set up fonts

print "* Loading fonts..."
pygame.font.init()
titlerowFont  = pygame.font.Font(titlerow_font_path, titlerow_font_size)
titlerowFont1 = titlerowFont
headlineFont  = pygame.font.Font(headline_font_path, headline_font_size)
clockitmFont  = pygame.font.Font(clockitm_font_path, clockitm_font_size)

# ...load the hourglass, shade image, etc

hourglass = pygame.image.load(hourglass_image_name).convert_alpha()
hourglass = pygame.transform.scale(hourglass, (128,128))
shadowbg  = pygame.image.load(shadowybg_image_name).convert_alpha()
shadowbg  = pygame.transform.smoothscale(shadowbg, (screen_width, shadowbg.get_height()))

startImage(loading_image_name, screen_width, screen_height)
time.sleep(1)
showLoading("Starting piFeed #"+ myID, "Created by Thomas Abrahamsson");

titlerowFont2 = pygame.font.Font(titlerow_font_path, int(titlerow_font_size*0.66))
bodytextFont  = pygame.font.Font(bodytext_font_path, bodytext_font_size)


def dirtify(thisRect):
    dirty_areas.append(thisRect)

# ...load overlay image

try:
   img_fg = pygame.image.load(overlay_image_name).convert_alpha()
except pygame.error, message:
   print 'Cannot load image:', overlay_image_name
   raise SystemExit, message

img_fg = pygame.transform.scale(img_fg, (screen_width, screen_height))
image_fg_rect = img_fg.get_rect()
img_bg = img_fg;
image_bg_rect = image_fg_rect
titlerow_pos = 0
bodytext_pos = 0
titlerow_msg = "Starting..."
bodytext_msg = ()
newsnode_msg = []
bodytime_msg = ""
feedname_msg = ""

def titlerowWrite(margin, posy, message):
    global titlerowFont,few_feed
    message = re_pattern.sub(u'\uFFFD', message)
    text = titlerowFont.render(message, True, WHITE)

    if ((margin + text.get_width()) > (screen_width - margin)):
       text = pygame.transform.smoothscale(text, (int(screen_width - margin * 2), int(text.get_height())))

    dirtify(windowSurface.blit(text, (margin,posy)))

def printTitlerow(message):
    global titlerow_pos, titlerow_msg,new_feed
    titlerow_pos = 0
    titlerow_msg = message

def bodytextWrite(bodytextLines, imgsurface, qrsurface):
    row = 0
    startX = screen_width*0.45
    startY = screen_height*0.30

    if (new_feed == True):
       text   = clockitmFont.render(bodytime_msg, True, GRAY)
       clockY = startY-text.get_height()*1.5
       dirtify(pygame.Rect(startX,clockY,int(startX+screen_width*0.45),int(screen_height-startY)))
       repaintBackground()
       windowSurface.blit(text, (startX, clockY))
       windowSurface.blit(imgsurface,(startX, startY))

    startY = startY + imgsurface.get_height()+10;
    intensity = 1

    for thisline in bodytextLines:
        row = row + 1
        if (row < (int(bodytext_pos))):
           if (startY<(screen_height*0.95)):
                pcolor = WHITE
                if (row > 10):
                    intensity = intensity + 0.5
                    pcolor = map(lambda x: int(x/intensity), WHITE)
                thisline = re_pattern.sub(u'\uFFFD', thisline)
                text = bodytextFont.render(thisline, True, pcolor)
                dirtify(windowSurface.blit(text, (startX, startY)))
                startY = startY + text.get_height()*1.05
    dirtify(windowSurface.blit(qrsurface, (int(screen_width-qrsurface.get_width()-screen_width*0.01), int(screen_height-qrsurface.get_height()-screen_height*0.01))))

def printBodyText(bodytext, timestmp):
    global bodytext_msg, bodytext_pos, bodytime_msg
    bodytextLines = wrapline(bodytext, bodytextFont, screen_width*0.45)
    bodytext_pos  = 0
    bodytext_msg  = bodytextLines
    bodytime_msg  = timestmp

def headlinesWrite():
    global newsnode_msg,f

    startX = screen_width*0.075
    startY = screen_height*0.27
    intensity = 1.0

    dirtify(pygame.Rect(startX,startY,int(startX+screen_width*0.27),int(screen_height-startY)))
    repaintBackground()

    for thisline in newsnode_msg:
        thisline = u"\u2022" + "" + thisline
        headtextLines = wrapline(thisline, headlineFont, screen_width*0.27)
        for headtext in headtextLines:
            if (startY < screen_height):
                    headtext = re_pattern.sub(u'\uFFFD', headtext)
                    text = headlineFont.render(headtext, True, BLACK)
                    windowSurface.blit(text, (startX+1, startY+1))
                    colornow = map(lambda x: int(x*intensity), WHITE)
                    text = headlineFont.render(headtext, True, colornow)
                    (windowSurface.blit(text, (startX, startY)))
                    startY = startY + text.get_height()*1.0
        startY = startY + 10
        if (intensity > 0):
            intensity = intensity - 0.075

def printHeadlines(news_nodes, nodeNow):
    global newsnode_msg
    newsnode_msg = []
    for i in range(nodeNow+1, (len(news_nodes))):
        newsnode_msg.append(news_nodes[i][1])
    for i in range(0, nodeNow):
        newsnode_msg.append(news_nodes[i][1])

def printMetaInfo():
    global feedname_msg
    timenow = datetime.now().strftime('%H:%M:%S')
    #timenow = datetime.strptime(datetime.now(), '%a %b %d %H:%M:%S GMT+01:00 %Y')
    locInfo = timenow + " ("+myIP+")"
    text = clockitmFont.render(locInfo, True, BLACK)
    locXY   = (screen_width-(text.get_width()*1.1), (text.get_height()*0.75))
    dirtify(windowSurface.blit(text, locXY))
    text = clockitmFont.render(locInfo, True, GRAY)
    dirtify(windowSurface.blit(text, map(lambda x: int(x-2), locXY)))
    
    timeLeft = (1 + nextrss_change - int(time.time()))
    fullHeader = feedname_msg+" ("+str(timeLeft)+")"
    sourceX = (screen_width*0.075);
    sourceY = (text.get_height()*0.75)
    text = clockitmFont.render(fullHeader, True, BLACK)
    dirtify(windowSurface.blit(text, ( sourceX+2, sourceY+1)))
    dirtify(windowSurface.blit(text, ( sourceX,   sourceY+2)))
    dirtify(windowSurface.blit(text, ( sourceX-2, sourceY+1)))
    dirtify(windowSurface.blit(text, ( sourceX+2, sourceY-1)))
    dirtify(windowSurface.blit(text, ( sourceX,   sourceY-2)))
    dirtify(windowSurface.blit(text, ( sourceX-2, sourceY-1)))
    text = clockitmFont.render(fullHeader, True, WHITE)
    dirtify(windowSurface.blit(text, ( sourceX, sourceY)))


def textsUpdate():
    global titlerow_pos, bodytext_pos
    if (titlerow_pos < len(titlerow_msg)):
        titlerow_pos = titlerow_pos+2
    titlerowWrite(screen_width*0.04, screen_height*0.110, titlerow_msg[0:titlerow_pos])
    bodytext_pos = bodytext_pos + 0.5
    bodytextWrite(bodytext_msg, news_nodes[news_node][3], news_nodes[news_node][5])
    printMetaInfo()

def loadRSS(feednr):
    global rss_feeds, feedname_msg,news_node, news_display_time, feedloops
    news_node = 0
    imgsurface = pygame.Surface((1,1), SRCALPHA)

    bgim = rss_feeds[feednr][3]

    # Support for downloaded background images, as well
    if (bgim[:4] == "http"):
       f_name = bgim.split('/')[-1]
       if (os.path.isfile("backgrounds/"+f_name) == False):
          print " * Downloading " + bgim + " (" + f_name + ")"
          f_name = "backgrounds/"+f_name;
          urllib.urlretrieve(bgim, f_name)
          bgim = f_name
       else:
          bgim = "backgrounds/"+f_name

    loadBackground(bgim)

    print "* Loading RSS Feed " + rss_feeds[feednr][1]
    feedname_msg = rss_feeds[feednr][1]
    
    news_display_time = rss_feeds[feednr][4]
    feedloops = rss_feeds[feednr][5]

    # instantiate the modified HTML parser and fed it some HTML
    lastImgFound = ""
    parser  = MyHTMLParser()
    myNodes = []
    rss_feed_url = rss_feeds[feednr][2]
    myIP = myID + " @ " + (get_ip_address('eth0') + " " + get_ip_address('wlan0')).strip()

    showLoading("piFeed Loading RSS, " + rss_feeds[feednr][1], myIP + " fetching news feed from "+rss_feed_url)

    d = feedparser.parse(rss_feed_url)

    if (len(d['feed'])==0):
       showLoading("No network? Cannot access feed for " + rss_feeds[feednr][1], myIP +" / Tried downloading from "+rss_feed_url)
       for i in range(1, 10):
          for event in pygame.event.get():
              if event.type == pygame.KEYDOWN:
                  if ((event.key == K_ESCAPE) or (event.key == K_q)):
                      pygame.quit()
                      sys.exit()
              if event.type == QUIT:
                  pygame.quit()
                  sys.exit()
          time.sleep(0.5)
       nextrss_change = 0
       #feedname_msg = d['feed']['title']

       getnewFeed()
    else:
       #print "* Failed. Error downloading feed. Removing it!"
       #showLoading("Error News Feed: " + rss_feeds[feednr][1], "Could not get News feed title. Remove!")
       #nextrss_change = 0
       #time.sleep(5.0)

       rss_entries  = d['entries']
       progressbar  = ""

       showLoading("Loading News Feed: " + rss_feeds[feednr][1], "Parsing News feed entries")

       print "* Parsing RSS Entries, Loading images, etc..."
       rss_entries = rss_entries[:10]; # 10 latest ones only
       for rss_entry in rss_entries:

          showLoading("Loading News Feed: " + rss_feeds[feednr][1], progressbar)

          progressbar = progressbar + "*"

          parser.reset()
          parser.feed(rss_entry['summary'])

          firstImage = ""

          # First, look for images in an alternative way, as embedded links
          if 'media_content' in rss_entry:
             firstImage = rss_entry.media_content[0]['url']
             print firstImage

          # If any images are found within the summary's HTML, we download it to a cache folder over HTTP...
          if ((len(parser.links)>0) or (len(firstImage)>0)):
              if (len(firstImage) == 0):
                 firstImage = parser.links[0];

              file_name = firstImage.split('/')[-1]
              file_hash = (hashlib.md5((firstImage).encode('utf-8')).hexdigest())

              if (os.path.isfile("cache/"+file_hash) == False):
                 firstImage = firstImage.replace("https", "http")
                 progressbar = progressbar + "."
                 print "* Downloading image..."
                 print "* Downloading image: " + firstImage
                 print "* Downloading image: " + file_hash

                 try:
                    urllib.urlretrieve(firstImage, "cache/"+file_hash)
                 except Exception, e:
                    print "* Download FAIL image!"
                    print "* Executing CURL fallback"
                    # Try CURL download as fallback
                    curlDLcmd="/usr/bin/curl -k -L '"+firstImage+"' > '/home/pi/pifeed/cache/"+file_hash+"'"
                    print "Executing " + curlDLcmd
                    os.system(curlDLcmd);
                    print "* CURL done"
                    progressbar = progressbar + "@"
              else:
                  progressbar = progressbar + "-"

              loadedExternalFile = False
              imgsurface = pygame.Surface((1,1), SRCALPHA)
              try:
                  imgsurface = pygame.image.load("cache/"+file_hash)
                  loadedExternalFile = True
              except pygame.error, message:
                  loadedExternalFile = False
                  progressbar = progressbar + "!"
                  print 'Cannot load image:', "cache/"+file_hash
                  os.remove("cache/" + file_hash)
                  print 'Deleted file:', "cache/" + file_hash

              #loadedExternalFile = False

              org_width  = imgsurface.get_width()
              new_height = 1
              new_width  = 1

              # Sometimes 1x1 pixels empty GIF's are published! We dont want these
              if (org_width > 1):
                  org_height = imgsurface.get_height()
                  scale_factor = org_width / (screen_width*0.47)
                  new_width   = int(org_width / scale_factor  )
                  new_height  = int(org_height / scale_factor)
              else:
                  loadedExternalFile = False

              try:
                 if (loadedExternalFile == True):
                    imgsurface = imgsurface.convert();
                    imgsurface = pygame.transform.smoothscale(imgsurface, (new_width, new_height))
              except ValueError:
                    imgsurface = pygame.transform.scale(imgsurface, (new_width, new_height))

              # Scale down /CROP/ too high images!

              if (new_height > (screen_height *0.40)):
                 imgsurface = pygame.transform.scale(imgsurface, (new_width, int(screen_height *0.40)))

              pygame.draw.rect(imgsurface, BLACK, (0,0,new_width, new_height), 3);


              #rssURL = parser.unescape(rss_entry['link'])
          rssURL = parser.unescape(rss_feeds[feednr][2])
          qrhash = "QR." + str(abs(hash(rssURL))) + ".png"

          if (os.path.isfile("cache/"+qrhash) == False):
              print "* Generating QR code"
              progressbar = progressbar + "Q"
              qr=QRCode() #version=20, error_correction=ERROR_CORRECT_L)
              qr.add_data(rssURL)
              qr.make(fit=True) # Generate the QRCode itself
              # im contains a PIL.Image.Image object
              QRtmp = qr.make_image()
              # To save it
              QRtmp.save("cache/" + qrhash)
              print "* QR saved to " + "cache/"+qrhash
          else:
              progressbar = progressbar + u"\u2022"

          # To load it
          QR = pygame.Surface((int(screen_width*0.1),int(screen_width*0.1)), SRCALPHA)
          try:
              QR = pygame.image.load("cache/" + qrhash)
              QR.convert()
              QR = pygame.transform.scale(QR, (int(screen_width*0.1),int(screen_width*0.1)))
          except pygame.error, message:
              QR = pygame.Surface((int(screen_width*0.1),int(screen_width*0.1)), SRCALPHA)
              print 'Cannot load image:', "cache/" + qrhash
              os.remove("cache/" + qrhash)
              print 'Deleted file:', "cache/" + qrhash

          publTime = ""
          extra = ""
          content = rss_entry['summary']
          if 'published' in rss_entry:
             publTime = rss_entry['published']
          if 'content' in rss_entry:
             content_entries = rss_entry['content']
             for content_entry in content_entries:
                if (hasattr(content_entry, 'value')):
                   content = content_entry.value
          content = content.replace('\n', ' ')
          content = content.replace('\r', '')
          myNodes.append((publTime, parser.unescape(rss_entry['title']), parser.unescape(content), imgsurface, rssURL, QR))
       print "* RSS stream loaded!"
       return myNodes

new_feed = True

def getnewFeed():
    global nextrss_change, news_nodes, rss_current_feed, feed_display_time
    global dirty_areas,cached_bg
    global new_feed
    if (nextrss_change <  int(time.time())):
        rss_current_feed = rss_current_feed + 1
        if (rss_current_feed >= len(rss_feeds)):
            rss_current_feed = 0
        dirty_areas = []
        cached_bg = None
        news_nodes = loadRSS(rss_current_feed)
        if not news_nodes:
            feed_display_time = 0;
        else:
            feed_display_time = (1+len(news_nodes)) * news_display_time * feedloops
        new_feed = True
        nextrss_change = int(time.time()) + feed_display_time

def updateNewsItems():
    global next_node_display,news_node, news_nodes
    global new_feed
    if ((news_nodes is None) or (len(news_nodes) == 0)):
        nextrss_change = int(time.time())
        #news_nodes = [None] * 1
        showLoading("Error Loading News Feed!", "Could not get News feed items. Remove!")
        new_feed = True
        nextrss_change = 0
        time.sleep(5.0)
        print "* Feed Load Error!"
    else:
        if (next_node_display == -1):
          next_node_display = int(time.time()) + news_display_time
          news_node = news_node - 1
          if (news_node < 0):
            news_node = (len(news_nodes)-1)
          refresh_headlines = True
        elif (next_node_display <  int(time.time())):
          next_node_display = int(time.time()) + news_display_time
          news_node = news_node + 1
          if (news_node >= len(news_nodes)):
	        news_node = 0
          refresh_headlines = True

	  printTitlerow(news_nodes[news_node][1])
  	  printBodyText(strip_tags(news_nodes[news_node][2]), news_nodes[news_node][0])
	  printHeadlines(news_nodes, news_node)
          new_feed = True

dirty_areas = []
cached_bg = None
windowSurface.fill((0,0,0))
getnewFeed()
new_feed = True

# Run the display loop
while True:
    repaintBackground()
    getnewFeed()
    updateNewsItems()
    if not ((news_nodes is None) or (len(news_nodes) == 0)):
       textsUpdate()
    if (new_feed == True):
       headlinesWrite()
       new_feed = False
    pygame.display.update()
    pygame.time.wait(20)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if ( (event.key == K_LEFT) or (event.key == K_BACKSPACE)):
                next_node_display = -1
            if ( (event.key == K_RIGHT) or (event.key == K_n)):
                next_node_display = 0
            if ((event.key == K_ESCAPE) or (event.key == K_q)):
                pygame.quit()
                sys.exit()
            if ((event.key == K_SPACE) or (event.key == K_p)):
               time.sleep(15)
            if ( (event.key == K_DOWN) or (event.key == K_RETURN)):
                nextrss_change = 0
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
