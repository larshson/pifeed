# piFeed

A python script that downloads and displays RSS feeds in an nice way.

## Getting Started

piFeed is only tested on a Raspberry pi.

### Prerequisites

Python requires pip and these packages installed:
pip install image
pip install feedparser
pip install qrcode

### Installing
Install Linux (Raspbian GNU/Linux 9) on a raspberry.
Rename the myid_example.txt to myid.txt - This identifies the unit
Rename the feed definitions files (remove) example and add the feeds wanted.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
test....